Image Replace File Entity
=============

Image Replace File Entity extends Image Replace (https://www.drupal.org/project/image_replace) to be able to work with file entity.
This comes especially useful when making image replace working with WYSIWYG editor.


A simple configuration:
------------------------------------------------

1. Enable module Image Replace and follow the README.txt to configure. Assume you create your picture mapping named as "Image Replace"

2. Specify image replace settings for file entity image bundle
     * go admin/structure/file-types/manage/image/fields, create image fields for replacement
     * go admin/structure/file-types/manage/image/edit, under Image Replace, assign image field to each image style.

3. Under image's display, admin/structure/file-types/manage/image/file-display
     * tick picture
     * Under display settings -> picture, select picture mapping to use Image Replace (the picture mapping you create in step 1)

Configuration to work with WYSIWYG editor:
----------------------------------------------------

1. Enable module Image Replace and follow the README.txt to configure. Assume you create your picture mapping named as "Image Replace"

2. Specify image replace settings for file entity image bundle
     * go admin/structure/file-types/manage/image/fields, create image fields for replacement
     * go admin/structure/file-types/manage/image/edit, under Image Replace, assign image field to each image style.

3. Under picture mapping settings, tick "Include Image Replace picture mapping in the CKEditor image dialog"
     * you don't have to use CKEDITOR, but we must tick this for some reason.

4. Under Text Formats, tick "Make images responsive with the picture module"

5. Finally, in WYSIWYG editor, for whichever image needs to be responsively replaced, just add attribute *data-picture-mapping="image_replace"* to the <img />
     * by using CKEDITOR, you will have the option to select a picture mapping through a drop down list. In other editor, this works only by adding it manually via HTML source code.
